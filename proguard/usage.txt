com.reyn.truedvorakpro.GestureHandler:
    static final int KEYCODE_OPTIONS
    static final int DpadSensitivity
com.reyn.truedvorakpro.LatinKeyboardView:
    static final java.lang.String TAG
    static final int KEYCODE_OPTIONS
    private android.view.View$OnTouchListener gestureListener
com.reyn.truedvorakpro.R
com.reyn.truedvorakpro.R$attr
com.reyn.truedvorakpro.R$color
com.reyn.truedvorakpro.R$dimen
com.reyn.truedvorakpro.R$drawable
com.reyn.truedvorakpro.R$id
com.reyn.truedvorakpro.R$integer
com.reyn.truedvorakpro.R$layout
com.reyn.truedvorakpro.R$string
com.reyn.truedvorakpro.R$xml
com.reyn.truedvorakpro.Settings:
    public static final java.lang.String PREFS_NAME
    public static final java.lang.String VIBRATE
    public static final boolean VIBRATE_DEF
    public static final java.lang.String V_DPAD
    public static final boolean V_DPAD_DEF
    public static final java.lang.String DDVORAK
    public static final boolean DDVORAK_DEF
    public static final java.lang.String PASSWORD
    public static final boolean PASSWORD_DEF
com.reyn.truedvorakpro.TrueDvorakKb:
    static final boolean DEBUG
    351:351:public boolean isFastkey(int)
