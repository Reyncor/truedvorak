/*
 * Copyright (C) 2008-2009 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.reyn.truedvorakpro;

import java.util.Arrays;

import android.content.Context;
import android.content.res.Resources;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Vibrator;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

/**
 * Example of writing an input method for a soft keyboard.  This code is
 * focused on simplicity over completeness, so it should in no way be considered
 * to be a complete soft keyboard implementation.  Its purpose is to provide
 * a basic example for how you would get started writing an input method, to
 * be fleshed out as appropriate.
 */
public class TrueDvorakKb extends InputMethodService 
        implements KeyboardView.OnKeyboardActionListener {
    
    /**
     * This boolean indicates the optional example code for performing
     * processing of hard keys in addition to regular text generation
     * from on-screen interaction.  It would be used for input methods that
     * perform language translations (such as converting text entered on 
     * a QWERTY keyboard to Chinese), but may not be used for input methods
     * that are primarily intended to be used for on-screen text entry.
     */

    private String TAG = "TDP ";
    static final boolean DEBUG = true;
    
    private LatinKeyboardView mInputView;
    
    private boolean mCapsLock;
    private long mLastShiftTime;
    private EditorInfo mFieldAttributes;
    private String mWordnoids;
    private int mFastkeys[] = {Keyboard.KEYCODE_DELETE}; //must be sorted
    
    private LatinKeyboard mSymbolsKeyboard;
    private LatinKeyboard mSymbolsShiftedKeyboard;
    private LatinKeyboard mDvorakKeyboard;
    private LatinKeyboard mDvorakShiftedKeyboard;
    private LatinKeyboard mPassDvorakKeyboard;
    private LatinKeyboard mPassDvorakShiftedKeyboard;
    
    //hardware
    private Vibrator vib;
    private LatinKeyboard mCurKeyboard;
    
    
    /**
     * Main initialization of the input method component.  Be sure to call
     * to super class.
     */
    @Override public void onCreate() {
        super.onCreate();
        mWordnoids = getResources().getString(R.string.wordnoids);
        
    }

    /**
     * This is the point where you can do all of your UI initialization.  It
     * is called after creation and any configuration change.
     */
    @Override public void onInitializeInterface() {
       
        mSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
        mSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
        Resources r = this.getResources();
        int mode_password_id = r.getIdentifier("com.reyn.truedvorakpro:integer/mode_password", null, null);
        if (DEBUG) Log.d(TAG+"password mode", String.valueOf(mode_password_id));
        if(!Settings.getLayout(this)){
	        mDvorakKeyboard = new LatinKeyboard(this, R.xml.dvorak);
	        mDvorakShiftedKeyboard = new LatinKeyboard(this, R.xml.dvorak_shift);
	        mPassDvorakKeyboard =  new LatinKeyboard(this, R.xml.dvorak, mode_password_id);
	        mPassDvorakShiftedKeyboard = new LatinKeyboard(this, R.xml.dvorak_shift, mode_password_id);
	    } else {
	        mDvorakKeyboard = new LatinKeyboard(this, R.xml.ddvorak);
	        mDvorakShiftedKeyboard = new LatinKeyboard(this, R.xml.ddvorak_shift);
	        mPassDvorakKeyboard =  new LatinKeyboard(this, R.xml.ddvorak, mode_password_id);
	        mPassDvorakShiftedKeyboard = new LatinKeyboard(this, R.xml.ddvorak_shift, mode_password_id);
	    }
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        
    }
    
    /**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     */
    @Override public View onCreateInputView() {
        mInputView = (LatinKeyboardView) getLayoutInflater().inflate(
                R.layout.input, null);
        mInputView.setOnKeyboardActionListener(this);
        mInputView.setKeyboard(mDvorakKeyboard);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        
        return mInputView;
    }
    
    /**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     */
    @Override public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        onInitializeInterface();
        
        // We are now going to initialize our state based on the type of
        // text being edited.
        mFieldAttributes = attribute;
        switch (mFieldAttributes.inputType&EditorInfo.TYPE_MASK_CLASS) {
        case EditorInfo.TYPE_CLASS_NUMBER:
        case EditorInfo.TYPE_CLASS_DATETIME:
            // Numbers and dates default to the symbols keyboard, with
            // no extra features.
            mCurKeyboard = mSymbolsKeyboard;
            break;
            
        case EditorInfo.TYPE_CLASS_PHONE:
            // Phones will also default to the symbols keyboard, though
            // often you will want to have a dedicated phone keyboard.
            mCurKeyboard = mSymbolsKeyboard;
            break;
            
        case EditorInfo.TYPE_CLASS_TEXT:
            mCurKeyboard = mDvorakKeyboard;
            int variation = mFieldAttributes.inputType &  EditorInfo.TYPE_MASK_VARIATION;
            if ((variation == EditorInfo.TYPE_TEXT_VARIATION_PASSWORD ||
                    variation == EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) && Settings.getLayoutPw(this)) {
            	mCurKeyboard = mPassDvorakKeyboard;
            }
            
            break;
            
        default:
            mCurKeyboard = mDvorakKeyboard; // Unknown, use standard.
	    }
	    
	    // Update the label on the enter key, depending on what the application
	    // says it will do.
        	mCurKeyboard.setImeOptions(getResources(), mFieldAttributes.imeOptions );
    }

    /**
     * This is called when the user is done editing a field.  We can use
     * this to reset our state.
     */
    @Override 
    public void onFinishInput() {
        super.onFinishInput();
        
        // We only hide the candidates window when finishing input on
        // a particular editor, to avoid popping the underlying application
        // up and down if the user is entering text into the bottom of
        // its window.
        setCandidatesViewShown(false);
        
        mCurKeyboard = mDvorakKeyboard;
        if (mInputView != null) {
            mInputView.closing();
        }
    }
    
    @Override 
    public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);
        // Apply the selected keyboard to the input view.
        mInputView.setKeyboard(mCurKeyboard);
        mInputView.closing();
    }

    @Override
    public boolean onEvaluateFullscreenMode(){
    	return false;
    }
    /**
     * Implementation of KeyboardViewListener, THIS IS THE BIG BOY
     */
    public void onKey(int keyCode, int[] keyCodes) {
    	Keyboard current = mInputView.getKeyboard();
    	boolean abortVib = false;
    	if (DEBUG) Log.d(TAG+"onKey", String.valueOf(keyCode));
        switch (keyCode) {
            case '\n':
                keyDownUp(KeyEvent.KEYCODE_ENTER);
                break;
            case Keyboard.KEYCODE_SHIFT:
            	handleShift();
            	break;
            case Keyboard.KEYCODE_DELETE:
            	keyDownUp(KeyEvent.KEYCODE_DEL);
            	break;
            case Keyboard.KEYCODE_MODE_CHANGE:
            	handleModeChange(current);
            	break;
            default:
                if (keyCode >= KeyEvent.KEYCODE_DPAD_UP && keyCode <= KeyEvent.KEYCODE_DPAD_RIGHT) {
                    keyDownUp(keyCode);
                    abortVib = true;
                } else if (isWordnoid(keyCode) && current == mSymbolsKeyboard) {
                	getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                } else {
	                getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
	                startMainKeyboard();
                }
                break;
        }
    	if (Settings.getVibrate(this) && !abortVib){
    		vib.vibrate(5);
    	}
    }
    
    private void handleModeChange(Keyboard current) {
        if (current == mSymbolsKeyboard || current == mSymbolsShiftedKeyboard) {
        	startMainKeyboard();
        } else {
            current = mSymbolsKeyboard;
            mInputView.setKeyboard(current);
        }
        if (current == mSymbolsKeyboard) { // sets the shift state to down when coming from symbols
            current.setShifted(false);
        }
	}

    private void handleShift() {
        if (mInputView == null) {
            return; //get out of here if, the world isn't quite right.
        } 			//makes us more flexible
        
        LatinKeyboard currentKeyboard = (LatinKeyboard) mInputView.getKeyboard();
        int currentMode = currentKeyboard.getModeId();
        if (mDvorakKeyboard == currentKeyboard) {

	            mInputView.setKeyboard(mDvorakShiftedKeyboard);
	            mInputView.setShifted(true);                                                                                   
        } else if (currentKeyboard == mDvorakShiftedKeyboard) {
        		checkToggleCapsLock();
            	mInputView.setKeyboard(mDvorakKeyboard);
            	mInputView.setShifted(false);

        } else if (currentKeyboard == mSymbolsKeyboard) {
            mSymbolsKeyboard.setShifted(true);
            mInputView.setKeyboard(mSymbolsShiftedKeyboard);
            mSymbolsShiftedKeyboard.setShifted(true);
        } else if (currentKeyboard == mSymbolsShiftedKeyboard) {
            mSymbolsShiftedKeyboard.setShifted(false);
            mInputView.setKeyboard(mSymbolsKeyboard);
            mSymbolsKeyboard.setShifted(false);
		} else if (mPassDvorakKeyboard == currentKeyboard) {
		        mInputView.setKeyboard(mPassDvorakShiftedKeyboard);
		        mInputView.setShifted(true);
		} else if (mPassDvorakShiftedKeyboard == currentKeyboard) {
		        mInputView.setKeyboard(mPassDvorakKeyboard);
		        mInputView.setShifted(false);
		}
    }

    private void startMainKeyboard (){
        switch (mFieldAttributes.inputType&EditorInfo.TYPE_MASK_CLASS) {
        case EditorInfo.TYPE_CLASS_NUMBER:
        case EditorInfo.TYPE_CLASS_DATETIME:
            // Numbers and dates default to the symbols keyboard, with
            // no extra features.
            mInputView.setKeyboard(mSymbolsKeyboard);
            break;
            
        case EditorInfo.TYPE_CLASS_PHONE:
            // Phones will also default to the symbols keyboard, though
            // often you will want to have a dedicated phone keyboard.
        	mInputView.setKeyboard(mSymbolsKeyboard);
            break;
            
        case EditorInfo.TYPE_CLASS_TEXT:
            int variation = mFieldAttributes.inputType &  EditorInfo.TYPE_MASK_VARIATION;
            if ((variation == EditorInfo.TYPE_TEXT_VARIATION_PASSWORD ||
                    variation == EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) && Settings.getLayoutPw(this)) {
            	mInputView.setKeyboard(mPassDvorakKeyboard);
            	if (DEBUG) Log.d(TAG+"smk pw", "true");
            } else {
            	mInputView.setKeyboard(mDvorakKeyboard);
            }
            
            break;
            
        default:
        	mInputView.setKeyboard(mDvorakKeyboard); // Unknown, use standard.
    }
    mInputView.setShifted(false);
    // Update the label on the enter key, depending on what the application
    // says it will do.
    mCurKeyboard.setImeOptions(getResources(), mFieldAttributes.imeOptions);
    }
    
    /**
     * Helper to send a key down / key up pair to the current editor.
     */
    private void keyDownUp(int keyEventCode) {
    	try {
    	getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_DOWN, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(
                new KeyEvent(KeyEvent.ACTION_UP, keyEventCode));
    	} catch (NullPointerException e){
    		Log.e(TAG+"keyUpDown", String.valueOf(keyEventCode));
    	}
    }
    
    private void checkToggleCapsLock() {
        long now = System.currentTimeMillis();
        if (mLastShiftTime + 800 > now) {
            mCapsLock = !mCapsLock;
            mLastShiftTime = 0;
        } else {
            mLastShiftTime = now;
        }
    }
    
    //Wordnoids are numbers, or characters that are on screens that should persist.
    //for example #1, 127.0.0.1, 9/11/2001 should all be easily typed.
    public boolean isWordnoid(int code) {
        return mWordnoids.contains(String.valueOf((char)code));
    }
    
    public boolean isFastkey(int code) {
        return Arrays.binarySearch(mFastkeys,code) >= 0;
    }
    
    public void onText(CharSequence text) {//needs to be implemented
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) return;		//seems to take text and process it before pasting it
        ic.beginBatchEdit();
        ic.commitText(text, 0);
        ic.endBatchEdit();
    }
    
    
    public void swipeRight() {
    }
    
    public void swipeLeft() {
    }

    public void swipeDown() {
    }

    public void swipeUp() {
    }
    
    public void onPress(int primaryCode) {
    	mInputView.onPress(primaryCode);
    }
    
    public void onRelease(int primaryCode) {
    	mInputView.onRelease(primaryCode);
    }
}
