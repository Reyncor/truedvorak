package com.reyn.truedvorakpro;

import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.GestureDetector.SimpleOnGestureListener;

public class GestureHandler extends SimpleOnGestureListener {

    static final int KEYCODE_OPTIONS = -100;

	LatinKeyboardView kbv;
	float dpi[]= new float[2];
	float dips[] = new float[2]; //density independent pixel scale
	float dads[] = new float[2]; //density adjusted dpad sensitivity
	int bufferK = -7;
	int bufferQ;
	boolean horizon;
	static final int DpadSensitivity = 10; 
	
	public GestureHandler(LatinKeyboardView ctx){
		kbv = (LatinKeyboardView) ctx;
		dpi[0] = 160;//kbv.getContext().getResources().getDisplayMetrics().xdpi;
		dpi[1] = 160;//kbv.getContext().getResources().getDisplayMetrics().ydpi;
		dips[0] = 160/dpi[0];
		dips[1] = 160/dpi[1];
		dads[0] = dips[0]*DpadSensitivity;
		dads[1] = dips[1]*DpadSensitivity;
		Log.d("TDP onScroll","Y dads="+String.valueOf((int)dads[1]) +" - X dads="+String.valueOf((int)dads[0]) );
		Log.d("TDP onScroll","Y dips="+String.valueOf((int)dips[1]) +" - X dips="+String.valueOf((int)dips[0]) );
	}
	
	@Override
	  public boolean onScroll(MotionEvent e1, MotionEvent e2, float dX, float dY) {
	    Log.d("TDP onScroll",String.valueOf((int)dX) +" - "+String.valueOf((int)dY) );
		if(kbv.getDkeyEngaged() && Settings.getVDpad(kbv.getContext())){
			kbv.setScrollPhase();
    		if(coordToDpad((int)-dX,(int)dY) == bufferK){
    			bufferQ+= horizon ? Math.abs((int)dX)*dips[0]: Math.abs((int)dY)*dips[1];
    		} else{
    			bufferK = coordToDpad((int)-dX,(int)dY);
    			horizon = bufferK == 0  || bufferK == 2;
    			Log.d("TDP onScroll","bufferK reset to="+String.valueOf(bufferK));
    		}
    		
    		if(bufferQ>=dads[0] && horizon){
    			turboD(coordToDpad((int)-dX,(int)dY),(int)(bufferQ/dads[0]));
    			bufferQ=0;
    		} else if(bufferQ>=dads[1] && !horizon){
    			turboD(coordToDpad((int)-dX,(int)dY),(int)(bufferQ/dads[1]));
    			bufferQ=0;
    		}
	    	return true;
		}else 
			return false;
	  }
	
	private int coordToDpad(int x, int y) {
		if (x==0 && y==0)
			return -1;
		
		int cv[] = new int[5];
		int result;
		
		if(x<0){
			cv[0] += x;
		} else if (x>0){
			cv[2] += x;
		}
		if(y>0){
			cv[1] += y;
		} else if (y<0){
			cv[3] += y;
		}
		cv[4]=Math.max(Math.max(Math.abs(cv[0]), cv[2]),Math.max(cv[1], Math.abs(cv[3])));
		
		if(cv[4]==Math.abs(x)){
			result = x==cv[0] ? 0 : 2;
		} else {
			result = y==cv[1] ? 1 : 3;
		}
		
		return result;
		
	}
	
	private void turboD(int kode, int n) {
		for(int i=0;i<Math.abs(n);i++){
    		switch (kode) {
			case 0:
				kbv.simulateKeyPress(KeyEvent.KEYCODE_DPAD_LEFT);
				Log.d("TDP key","Transmitting left");
				break;
			case 1:
				kbv.simulateKeyPress(KeyEvent.KEYCODE_DPAD_UP);
				Log.d("TDP key","Transmitting up");
				break;
			case 2:
				Log.d("TDP key","Transmitting right");
				kbv.simulateKeyPress(KeyEvent.KEYCODE_DPAD_RIGHT);
				break;
			case 3:
				Log.d("TDP key","Transmitting down");
				kbv.simulateKeyPress(KeyEvent.KEYCODE_DPAD_DOWN);
				break;
			default:
				Log.d("TDP key","Transmitting center");
				kbv.simulateKeyPress(KeyEvent.KEYCODE_DPAD_CENTER);
				break;
			}
		}
	}

}
