/**
 * All rights reserved.
 * @author Reynaldo Cortorreal
 */

package com.reyn.truedvorakpro;

import java.util.Arrays;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class LatinKeyboardView extends KeyboardView {
	
    static final String TAG = "TDP ";
    static final int KEYCODE_OPTIONS = -100;
	
    private GestureDetector gestureDetector;
    private GestureHandler gestureHandler;
    private View.OnTouchListener gestureListener;
    private MotionEvent newmo;
    private Bitmap dpad_pic;
    private int finger[] = {0,0};
    private Paint blank; 
    private boolean dKeyEngaged = false;
    private boolean scrollPhase= false;

    public LatinKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initHelper();
    }
    
    public LatinKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initHelper();
    }
    
    public void initHelper(){
        setBackgroundColor(0xFF000000);
        gestureHandler = new GestureHandler(this);
        gestureDetector = new GestureDetector(this.getContext(), gestureHandler);
        
        dpad_pic = BitmapFactory.decodeResource(getResources(),
				R.drawable.sym_keyboard_shift);
		Paint shadow = new Paint();
    }

    public void simulateKeyPress(int key) {
    	getOnKeyboardActionListener().onKey(key , null);
    }
    
    @Override
    protected boolean onLongPress(Key key) {
        boolean result;
    	if (key.codes[0] == Keyboard.KEYCODE_MODE_CHANGE) {
    		Intent i = new Intent(getContext(), Settings.class);
    		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
    		getContext().startActivity(i);
    		result = true;
        } else if (key.codes[0] == 'd') {
            getOnKeyboardActionListener().onKey(KeyEvent.KEYCODE_DPAD_CENTER, null);
            result = true;
        } else {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
            result = super.onLongPress(key);
        }
        return result;
    }
    
    @Override
	public boolean onTouchEvent(MotionEvent e) {
        if(gestureDetector.onTouchEvent(e)){
        	//this.setPreviewEnabled(false);
        	newmo = MotionEvent.obtain(e.getDownTime(), e.getEventTime(), MotionEvent.ACTION_CANCEL, e.getRawX(), e.getRawY(), e.getMetaState());
        	finger[0]=(int)e.getRawX(); 
        	finger[1]=(int)e.getRawY();
        	invalidate();
        	return super.onTouchEvent(newmo);
        	//return true;
        }else if(scrollPhase && e.getAction() == MotionEvent.ACTION_UP){
	    		setDkeyEngaged(false);
	    		scrollPhase=false;
	        	newmo = MotionEvent.obtain(e.getDownTime(), e.getEventTime(), MotionEvent.ACTION_CANCEL, 0, 0, e.getMetaState());
	        	invalidate();
	        	return super.onTouchEvent(newmo);
    	}else {
    		Log.w(TAG+"TouchDown","motion action: "+e.getAction());
    		return super.onTouchEvent(e);
    	}
    }
    
    @Override
    public void onDraw(Canvas canvas){
    	super.onDraw(canvas);
		
		if(scrollPhase) 
			canvas.drawBitmap(dpad_pic, finger[0], finger[1]-20-this.getHeight(), blank);

		// Draw the vertical grid lines
		/*for (int i = 0; i <= 5; i++) {
			canvas.drawLine(i * 16-1, 0, i * 16-1, 10, shadow);
		}*/
    }
    
	public boolean getDkeyEngaged() {
		return dKeyEngaged;
	}
	
	public void setDkeyEngaged(boolean b) {
		dKeyEngaged = b;
	}
	
	public void setScrollPhase() {
		scrollPhase = true;
	}
	
    public void onPress(int primaryCode) {
    	if(primaryCode == 'd'){
    		dKeyEngaged = true;
    	}
    }
    
    public void onRelease(int primaryCode) {
    }
    
}

