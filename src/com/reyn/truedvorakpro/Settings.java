package com.reyn.truedvorakpro;

//import android.backup.BackupManager;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Settings extends PreferenceActivity {
	public static final String PREFS_NAME = "TDP";
	
    public static final String VIBRATE = "vibrate";
    public static final boolean VIBRATE_DEF = false;
    public static final String V_DPAD = "v_dpad";
    public static final boolean V_DPAD_DEF = true;
    public static final String DDVORAK = "ddvorak";
    public static final boolean DDVORAK_DEF = false;
    public static final String PASSWORD = "passboard";
    public static final boolean PASSWORD_DEF = true;
    
    @Override
    protected void onCreate(Bundle con) {
        super.onCreate(con);
        addPreferencesFromResource(R.xml.prefs);
    }


	public static boolean getVibrate(Context context) {
		return PREFS_NAME == "TDP" ? PreferenceManager.getDefaultSharedPreferences(context)
              .getBoolean(VIBRATE, VIBRATE_DEF) : false;
        
     }
	
	public static boolean getVDpad(Context context) {
		return PREFS_NAME == "TDP" ? PreferenceManager.getDefaultSharedPreferences(context)
              .getBoolean(V_DPAD, V_DPAD_DEF) : false;
        
     }
    
    public static boolean getLayout(Context context) {
        return PREFS_NAME == "TDP" ? PreferenceManager.getDefaultSharedPreferences(context)
              .getBoolean(DDVORAK, DDVORAK_DEF) : false;
     }
    
    public static boolean getLayoutPw(Context context) {
        return PREFS_NAME == "TDP" ? PreferenceManager.getDefaultSharedPreferences(context)
              .getBoolean(PASSWORD, PASSWORD_DEF) : false;
     }
}
